local tileset = class("tileset"){
    tileset = {}
}:reg()

function tileset:has_tile(name)
    return self.tileset[name] ~= nil
end 

function tileset:add_tile(name,imgassets)
    self.tileset[name] = imgassets
end

function tileset:get_tile(name)
    return self.tileset[name]
end

function tileset:draw_tile(name,...)
    if self:has_tile(name) then
        local ass = core.assets.image(self.tileset[name])
        love.graphics.draw(ass,...)
    end
end

return atlas





