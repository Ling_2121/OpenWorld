local signal_unit = class("SignalUnit"){
    connects = {},
}

function signal_unit:connect(object,cnt_func_name)
    if self.connects[object] == nil then
        self.connects[object] = setmetatable({},{__mode = "kv"})
    end
    self.connects[object][cnt_func_name] = cnt_func_name
end

function signal_unit:disconnect(object,cnt_func_name)
    if self.connects[object] ~= nil then
        self.connects[object][cnt_func_name] = nil
        if next(self.connects[object]) == nil then
            self.connects[object] = nil
        end
    end
end

function signal_unit:emit(...)
    for object,all_connect in pairs(self.connects) do
        for func_name in pairs(all_connect) do
            object[func_name](object,...)
        end
    end
end

local signal = component("Signal"){
    signals = {},
}:reg()

function signal:signal(name)
    self.signals[name] = signal_unit()
end

function signal:connect(sig,obj,to_func)
    if self.signals[sig] ~= nil then
        self.signals[sig]:connect(obj,to_func)
    end
end

function signal:disconnect(sig,obj,from_func)
    if self.signals[sig] ~= nil then
        self.signals[sig]:disconnect(obj,from_func)
    end
end

function signal:emit_signal(sig,...)
    if self.signals[sig] ~= nil then
        self.signals[sig]:emit(...)
    end
end

return signal