local core = class("Core"){
    user_name = "LaoHong",
    network_ip = "127.0.0.1",
    network_port = 23333,
    core_mode = CORE_MODE_CLIENT,
    game_mode = GAME_MODE_LOCAL,
    players = {},
}

function core:is_local_game()
    return self.game_mode == GAME_MODE_LOCAL
end

function core:perp_init()
    self.registry = require("core/module/registry/registry")()
    self.registry:add_registry("default")
    :add_registry("block")
    :add_registry("item")
    :add_registry("life")
    :add_registry("world")
end

function core:init()
    self.network = require("core/module/network/network")()
    self.input = require("core/module/input")()
    self.grun = require("core/module/grun/grun")()
    self.filesys = require("core/module/filesys")()
    self.assets = require("core/module/assets")()
    self.game_mode = GAME_MODE_LOCAL
end

function core:get_mouse_position()
    local mx,my = love.mouse.getPosition()
    local world = self.grun:get_scene()
    if world then
        return world.camera:toWorldCoords(mx,my)
    else
        return mx,my
    end
end

--获取注册的对象类
function core:get_regobject(type,reg_name)
    return self.registry:get_regobj(type,reg_name)
end

return core