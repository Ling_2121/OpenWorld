local registry = class("Registry"){
    registry = {}
}

function registry:add_registry(name)
    if self.registry[name] == nil then
        self.registry[name] = {}
    end
    return self
end

function registry:is_regobj(registry_name,reg_name)
    local registry = self.registry[registry_name]
    if not registry then return false end
    return registry[reg_name] ~= nil
end

function registry:registered(registry_name,reg_name,object)
    local registry = self.registry[registry_name]
    if registry then
        registry[reg_name] = object
    end
    return self
end

function registry:get_registry(registry_name)
    return self.registry[registry_name]
end

function registry:get_regobj(registry_name,reg_name)
    local registry = self.registry[registry_name]
    if registry then
        return registry[reg_name]
    end
end

return registry