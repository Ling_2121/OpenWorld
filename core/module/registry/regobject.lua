local regobject = component("Regobject"){
    __reg_type__ = "default",
    __reg_name__ = "Regobject"
}

function regobject:__reg__(reg_name,reg_type)
    self.__reg_name__ = reg_name or self.__name__
    self.__reg_type__ = reg_type or "default"
    core.registry:registered(self.__reg_type__,self.__reg_name__,self)
end

return regobject



