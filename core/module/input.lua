local input = class("Input"){
    _inputs = {},
}:reg()

function input:add_input(name,input)
    input.is_key_press = false
    input.is_key_release = false
    input.is_mouse_press = false
    input.is_mouse_release = false
    self._inputs[name] = input
end

function input:is_press(name)
    local struct = self._inputs[name]
    if struct then
        return struct.is_key_press or struct.is_mouse_press
    end
    return false
end

function input:is_release(name)
    local struct = self._inputs[name]
    if struct then
        return struct.is_key_release or struct.is_mouse_release
    end
    return false
end

function input:update()
    for name,struct in pairs(self._inputs) do
        if struct.key then
            local iskdown = love.keyboard.isDown(unpack(struct.key))
            if struct.is_key_press and not iskdown then
                struct.is_key_release = true
            else
                struct.is_key_release = false
            end
        end

        if struct.mouse then
            local ismdown = love.mouse.isDown(unpack(struct.mouse))

            if struct.is_mouse_press and not ismdown then
                struct.is_mouse_release = true
            else
                struct.is_mouse_release = false
            end
        end
    end

    for name,struct in pairs(self._inputs) do
        if struct.key then
            struct.is_key_press = love.keyboard.isDown(unpack(struct.key))
            if struct.is_key_press == true then
                struct.is_key_release = false
            end
        end

        if struct.mouse then 
            struct.is_mouse_press = love.mouse.isDown(unpack(struct.mouse))
            if struct.is_mouse_press == true then
                struct.is_mouse_release = false
            end
        end
    end
end

return input