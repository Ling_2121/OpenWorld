local sock = require"library.sock.sock"
local bitser = require"library.bitser"

local client = class("Client",oopsys.getcls("Signal")){
    connect_ip = "127.0.0.1",
    connect_port = "23333",
    sock = nil,
}

function client:__init__(server_ip,server_port)
    self.connect_ip = server_ip or "127.0.0.1"
    self.connect_port = server_port or "23333"
    self.sock = sock.newClient(self.connect_ip,self.connect_port)
    self.sock:setSerialization(bitser.dumps, bitser.loads)
    self.sock:on("connect",function()
        print("Connect to server")
        self:emit_signal("connect")
    end)

    self.sock:on("disconnect",function()
        print("Disconnect to server")
        self:emit_signal("disconnect")
    end)

    self:signal("connect")
    self:signal("disconnect")
end

function client:connect_server(server_ip,server_port)
    self.connect_ip = server_ip or core.network_ip
    self.connect_port = server_port or core.network_port
    self.sock.address = self.connect_ip
    self.sock.port = self.connect_port
    self.sock:connect()
end

function client:update(dt)
    if not core:is_local_game() then
        self.sock:update(dt)
    end
end

return client