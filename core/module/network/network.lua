local Client = require"core/module/network/client"
local Server = require"core/module/network/server"
local ClientObject = require"core/module/network/client_object"

local network = class("Network"){
    sockport = nil,
    clients = {},
    event_registry = {},
}

function network:__init__()
    self:init_sockport()
end

function network:init_sockport()
    local sock = require"library.sock.sock"
    if core.core_mode == CORE_MODE_CLIENT then
        self.sockport = Client(core.network_ip,core.network_port)
    end
    if core.core_mode == CORE_MODE_SERVER then
        self.sockport = Server(core.network_ip,core.network_port)
        -- self.sockport.sock:on("connect",function(peer)
        --     self.clients[peer] = ClientObject()
        -- end)
    end
end

function network:select_client(ax,ay)
    local select = {}
    local area = core.grun:get_scene():get_area(x,y)
    for client in pairs(area.clients) do
        table.insert(select,client)
    end
    return select
end

function network:is_regreq(req_name)
    return self.request_registry[req_name] ~= nil
end

function network:select_client_from_center(cax,cay)
    local select = {}
    local ax,ay = cax - 1,cay - 1
    for x = ax,x + 2 do
        for y = ay,y + 2 do
            local area = core.grun:get_scene():get_area(x,y)
            for client in pairs(area.clients) do
                table.insert(select,client)
            end
        end
    end
    return select
end

function network:registered_event(name)
    local event = {}
    local registry = {
        request  = {run = nil,schemas = nil},
        feedback = {run = nil,schemas = nil},
    }
    self.event_registry[name] = registry
    event.request = function(self,schemas,func)--服务器收到请求时
        if core.core_mode == CORE_MODE_SERVER then
            registry.request.run = func
            registry.request.schemas = schemas
            self.sockport.sock:setSchema(name,schemas)
            self.sockport.sock:on(name,function(pack,peer,orgn_pack)
                func(pack,peer,self.clients,orgn_pack)
            end)
        end
        return self
    end

    event.feedback = function(self,schemas,func)--客户端执行的反馈
        registry.feedback.run = func
        registry.feedback.schemas = schemas
        if core.core_mode == CORE_MODE_CLIENT then
            self.sockport.sock:setSchema(req_name,schemas)
            self.sockport.sock:on(name,function(pack,peer,orgn_pack)
                func(pack,peer)
            end)
        end
        return self
    end

    event.reg = function(self)
        assert(registry.request.run == nil,"必须定义请求！")
        if registry.feedback.run == nil then
            self:feedback(registry.request.schemas,registry.request.run)
        end
    end

    return event
end

function network:request_event(name,pack)
    if not self.event_registry[name] then return end
    if core.core_mode == CORE_MODE_CLIENT then
        self.sockport.sock:send(name,pack)
    end
end

---只在反馈端使用--------------------------------------------
function network:send_event_to_all(name,orgn_pack)
    self.sockport:broadcast_event(name,orgn_pack)
end

function network:send_event_to_peer(peer,name,pack)
    peed:send(name,pack)
end

function network:send_event_to_select(select,name,pack)
    for i,client_name in ipairs(select) do
        local client = clients[client_name]
        if client then
            client.peer:send(name,pack)
        end
    end
end
-----------------------------------------------------------

return network