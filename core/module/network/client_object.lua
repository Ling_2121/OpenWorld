local client_object = class("ClientObject"){
    -- peer = nil, --服务端才有
    x = 0,
    y = 0,
}

if core.core_mode == CORE_MODE_SERVER then
    function client_object:__init__(peer,x,y)
        self.peer = peer
        self.x = x or 0
        self.y = y or 0
    end
end

if core.core_mode == CORE_MODE_CLIENT then
    function client_object:__init__(x,y)
        self.x = x or 0
        self.y = y or 0
    end
end

return client_object