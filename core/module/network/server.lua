local sock = require"library.sock.sock"
local bitser = require"library/bitser"

local server = class("Server",oopsys.getcls("Signal")){
    ip = nil,
    port = nil,
    sock = nil,
}

function server:__init__(ip,port)
    self.sock = sock.newServer(ip,port)
    self.sock:setSerialization(bitser.dumps, bitser.loads)
    self.sock:on("connect",function(data,client)
        print(client,"Connect!")
        self:synchronize(client)
    end)
end

function server:synchronize(client)
    local world = core.grun:get_scene()
    for name,obj in pairs(world.objects) do
        if oopsys.is(obj,"WorldNetworkObject") then
            
        end
    end
end

function server:broadcast_request(name,data)
    self.sock:sendToAll(name,data)
end

function server:update(dt)
    self.sock:update(dt)
end

return server
