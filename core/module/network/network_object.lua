local Regobject = require"core/module/registry/regobject"

local network_object = component("NetworkObject",Regobject){
    __self_events__ = {}
}

function network_object:get_event_name(name)
    return self.__self_events__[name]
end

function network_object:create_event(name)
    local event_name = string.match("%s_%s_%s",self.__reg_type__,self.__reg_name__,name)
    self.__self_events__[name] = event_name
    return core.network:registered_event(event_name)
end

function network_object:request_event(name,pack)
    local event_name = self:get_event_name(name)
    if not event_name then return end
    core.network:request_event(event_name,pack)
end

return network_object

