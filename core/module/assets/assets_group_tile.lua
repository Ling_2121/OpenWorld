local AssetsGroup = require"core.module.assets.assets_group"

local assets_group_tile = class("AssetsGrouptile",AssetsGroup){}:reg()

function assets_group_tile:auto_load(path)
    if not path then return self end
    local items = core.filesys.get_all_file_item(path)
    
    for i,item in ipairs(items) do
        if item.type == "image" then
            local name = item.name:match("([0-9a-zA-z_]+)%.png")
            self:add(name,"image",item.path)
        end
    end
    return self
end

return assets_group_tile