local assets = class("AssetsUnit"){
    assets = {},
    group = {},
}

function assets:__init__(load_func)
    self.load = load_func or self.load
end

function assets:load(path,...)end

function assets:add(name,item)
    self.assets[name] = item
end

function assets:start()
    self.__call = function(self,name,...)
        local a = self.assets[name]
        if not a then
            a = {
                data = self.load(name,...),
                path = name,
            }
            self.assets[name] = a
        end
        return a.data
    end
end

local assets_manager = class("AssetsManager"){}:reg()

function assets_manager:__init__()
    self:registered_assets("image",assets(love.graphics.newImage)):start();
    self:registered_assets("audio",assets(love.audio.newSource)):start();
    self:registered_assets("font",assets(love.graphics.newFont)):start();
    self:registered_assets("group",class("AssetsGroupGroup",assets){
        start = function(self)
            self.__call = function(self,name)
                return self.assets[name]
            end
        end
    }):start()
end

function assets_manager:registered_assets(type,assets)
    self[type] = assets
    return assets
end

function assets_manager:has_group(name)
    return self.group[name] ~= nil
end

function assets_manager:add_group(name,group)
    self.group:add(name,group)
    return group
end

function assets_manager:remove_group(name)
    self.group:remove(name)
end

function assets_manager:get_group(name)
    return self.group:get(name)
end

return assets_manager