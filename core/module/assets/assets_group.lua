local assets_group = class("AssetsGroup"){
    assets = {},
}:reg()

function assets_group:add(name,type,assets)
    self.assets[name] = {
        type = type,
        assets = assets,
    }
    return self
end

function assets_group:remove(name)
    self.assets[name] = nil
end

function assets_group:get(name)
    local a = self.assets[name]
    if a then
        return a.assets
    end
end

function assets_group:get_assets(name)
    local a = self.assets[name]
    if a then
        local assets = core.assets[a.type]
        return assets(a.assets)
    end
end

return assets_group

