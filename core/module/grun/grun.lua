local grun = class("GameRun",oopsys.getcls("Signal")){
    scene = nil,
    scenes = {},
}:reg()

do
    for _,name in ipairs(LOVE_CALLBACK) do
        if grun[name] == nil then
            grun[name] = function(self,...)
                local scene = self.scenes[self.scene]
                if scene then
                    if scene[name] then
                        scene[name](scene,...)
                    end
                end
            end
        end
    end
end

function grun:__init__()
    self:signal("change_scene")
end

function grun:start()
    for _,name in ipairs(LOVE_CALLBACK) do
        love[name] = function(...)
            self[name](self,...)
        end
    end
    love.update = function(dt)
        core.network.sockport:update()
        core.input:update()
        self:update(dt)
    end
end

function grun:get_scene(name)
    return self.scenes[name or self.scene or ""]
end

function grun:add_scene(name,scene)
    self.scenes[name] = scene
end

function grun:remove_scene(name)
    self.scenes[name] = nil
    if self.scene == name then
        self:change_scene(nil)
    end
end

function grun:change_scene(name)
    if self.scene ~= nil then
        local scene = self:get_scene()
        if scene then
            if scene._exit_run then
                scene:_exit_run()
            end
        end
    end
    self.scene = name
    local scene = self:get_scene()
    if scene then
        if scene._init_status ~= true and type(scene._init) == "function" then
            scene:_init()
            scene._init_status = true 
        end
        if scene._enter_run then
            scene:_enter_run()
        end
    end
    self:emit_signal("change_scene",name)
end

return grun
