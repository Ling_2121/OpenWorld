local Signal = oopsys.getcls("Signal")
local NetworkObject = require"core.module.network.network_object"

local scene_node = class("SceneNode",Signal,NetworkObject){
    name = nil,
    depth = 0,
    x = 0,
    y = 0,
    _area = nil,
}

function scene_node:__init__(name,x,y)
    self.name = name or tostring(self)
    self.x = x or x
    self.y = y or y
    self.depth = self.y
end

function scene_node:__reg_event__()
    self:create_event("create")
    :request({"name","x","y"},function(pack,peer,clients,orgn_pack)
        local scene = core.grun:get_scene()
        local network = core.network
        local obj = self(pack.name,pack.x,pack.y)
        scene:add_object(obj)
        local select = network:select_client(scene:wposi_to_aposi())
        network:send_event_to_select(select,pack,orgn_pack)
    end)
    :feedback({"name","x","y"},function(pack,peer,orgn_pack)
        local scene = core.grun:get_scene()
        local obj = self(pack.name,pack.x,pack.y)
        scene:add_object(obj)
    end)
    :reg()

    self:create_event("delete")

    self:create_event("synchronize")
    :request({"name"},function(pack,peer,clients,orgn_pack)
        local scene = core.grun:get_scene()
        local object = scene:get_object(pack.name)
        local event_name = object:get_event_name("synchronize")
        peer:send(event_name,object:__synchronize_data__())
    end)
    :feedback({"name","x","y"},function(pack)
        local scene = core.grun:get_scene()
        local object = scene:get_object(pack.name)
        if not object then
            object = self()
            scene:add_object(object)
        end
        object:__synchronize__(pack)
    end)
    :reg()

    self:create_event("move")
end

--返回同步的数据
function scene_node:__synchronize_data__()
    return {
        self.name,
        self.x,
        self.y,
    }
end

--进行同步
function scene_node:__synchronize__(pack)
    local scene = core.grun:get_scene()
    scene:moveto_object(self,pacl.x,pack.y)
end

function scene_node:draw(camera)
    
end

return scene_node:reg()



