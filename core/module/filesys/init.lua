local biser = require"library.bitser"

local filesys = class("FileSys"){
    --[[
        文件夹结构
        [ospath]
            [game]
                (config.ini)
                [worlds]
                    [world]
                        [areas]
                            (area.dat{
                                x,
                                y,
                                objects = {
                                    ...object... #file name
                                }
                                tiles = {}
                            })
                        [objects /]
                            (object.dat{
                                ...object_data...
                            })
                        (world.dat{
                            ...world_data...
                        })
                [assets /]
                [modes /]
    --]]
}

function filesys.get_all_file_item(dir,tb)
    tb = tb or {}
    local fitem = love.filesystem.getDirectoryItems(dir)

    local match_table = {
        {"([0-9a-zA-z_]+)%.lua", "lua_file",".lua"},-- lua文件       lua_file
        {"([0-9a-zA-z_]+%.png)", "image",".png"},-- 图像文件      image
        {"([0-9a-zA-z_]+%.ogg)", "audio",".ogg"},-- 音频          audio
        {"([0-9a-zA-z_]+%.otf)", "font",".otf"} -- 字体          font
    }

    for k,name in ipairs(fitem) do
        local p = dir.."/"..name
        local type =  love.filesystem.getInfo(p).type

        for i,matstr in ipairs(match_table) do
            local mat = matstr[1]
            local t = matstr[2]
			local n = name:match(mat) 
			if n then
                table.insert(tb,{
                    name = name,
                    type = t,
                    path = dir .. "/" .. n
				})
				break
            end
        end

        if type == "directory" then
            filesys.get_all_file_item(p,tb)
        end
	end

    return tb
end

function filesys:save_object(serobj)

end

function filesys:load_object(file_name)

end

function filesys:sava_file(file)

end

function filesys:load_file(file_name)

end

return filesys