local serobject = component("SerObject"){
    save_path = "/",
    save_name = "object.dat"
}

--返回要存储的数据
function serobject:__dump__()
    return {}
end

function serobject:save()
    local data = self:__dump__()
    core.filesys:save_object(self)
end

function serobject:load(data)
    
end

return serobject