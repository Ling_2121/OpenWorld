LOVE_CALLBACK = {
    "update",
    "draw",
    "keypressed",
    "keyreleased",
    "mousemoved",
    "mousepressed",
    "mousereleased",
    "wheelmoved",
    "textedited",
    "textinput",
}

CORE_MODE_CLIENT = 1
CORE_MODE_SERVER = 2

GAME_MODE_LOCAL = 1
GAME_MODE_NETWORK = 2

WORLD_AREA_SIZE = 8
WORLD_GRID_PSIZE = 16
WORLD_AREA_PSIZE = WORLD_AREA_SIZE * WORLD_GRID_PSIZE

ASSETS_TYPE_IMAGE = "image"
ASSETS_TYPE_AUDIO = "audio"
ASSETS_TYPE_FONT  = "font"

AREA_STATUS_LOADING = 0
AREA_STATUS_STOP = 1
AREA_STATUS_UNLOAD = 2

REQUEST_TYPE_TOSERVER = 1--只到服务端
REQUEST_TYPE_TOALL = 2--到所有
