oopsys.registered_all("scene/test_scene2/objects")

local World = oopsys.getcls("World")

local WorldArea = require"object.world.area"
local test_scene2 = class("TestScene2",World){}
local Player = oopsys.getcls("Player")

function test_scene2:__not_area__(world,ax,ay)
    local area = WorldArea(ax,ay)
    world:add_area(area)
end

if core.core_mode == CORE_MODE_CLIENT then
    function test_scene2:_init()
        core.network.sockport:connect_server()
        core.game_mode = GAME_MODE_NETWORK
        core.input:add_input("move_up",{key={"w"}})
        core.input:add_input("move_down",{key={"s"}})
        core.input:add_input("move_left",{key={"a"}})
        core.input:add_input("move_right",{key={"d"}})
        core.network.sockport:connect("connect",self,"__connect_to_server__")
    end

    function test_scene2:__connect_to_server__()
        local player = Player(core.user_name)
        player.name = core.user_name
        player:create()
    end

    function test_scene2:update(dt)
        World.update(self,dt)
        local player = self:get_object(core.user_name)
        if player then
            self:set_loading_center_from_world_poaition(player.x,player.y)
        end
    end

    function test_scene2:draw()
        World.draw(self)
        love.graphics.print(string.format("%d %d",self.loading_center.x,self.loading_center.y))
    end
end

return test_scene2()