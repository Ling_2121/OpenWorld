local Life = require"scene/test_scene2/objects/life"

local player = class("Player",Life){}

if core.core_mode == CORE_MODE_CLIENT then
    function player:update(dt)
        if self.name ~= core.user_name then return end
        local world = core.grun:get_scene()
        local vx,vy = 0,0

        if core.input:is_press("move_up") then
            vy = -self.move_speed
        end
        
        if core.input:is_press("move_down") then
            vy = self.move_speed
        end

        if core.input:is_press("move_left") then
            vx = -self.move_speed
        end

        if core.input:is_press("move_right") then
            vx = self.move_speed
        end
        if vx ~= 0 or vy ~= 0 then
            self:send_request(REQUEST_TYPE_TOALL,"move",self.name,vx * dt,vy * dt)
            self.depth = math.ceil(self.y)
        end
    end
end

return player:reg()