local Life = require"scene/test_scene2/objects/life"

local player2 = class("Player2",Life){
    dir = {x = 0, y = 0},
    time = 3,
    timer = 0,
}

if core.core_mode == CORE_MODE_CLIENT then
    function player2:update(dt)
        self.timer = self.timer + dt
        if self.timer >= self.time then
            self.timer = 0
            self.dir.x = love.math.random(-30,30)
            self.dir.y = love.math.random(-30,30)
        end
        self:send_request(REQUEST_TYPE_TOALL,"move",self.name,self.dir.x * dt,self.dir.y * dt)
    end
end

return player2:reg()