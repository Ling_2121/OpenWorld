local WorldNetworkObject = oopsys.getcls("WorldNetworkObject")--require"object.world_network_object"

local life = class("Life",WorldNetworkObject){
    depth = 0,
    x = 0,
    y = 0,
    name = "",
    move_speed = 100,
}

function life:__init__(name)
    self.name = name or tostring(self)
end

function life:__reg_netreq__()
    self:create_event("create",{"name","x","y"},function(pack)
        local world = core.grun:get_scene()
        local obj = self()
        obj.name = pack.name
        obj.x = pack.x
        obj.y = pack.y
        world:add_object(obj,obj.name)
    end)

    self:create_event("move",{"self","vx","vy"},function(self,pack)
        local world = core.grun:get_scene()
        world:move_object(self,pack.vx,pack.vy)
        self.depth = self.y
    end)

    self:create_event("synchronize",{"name","x","y","move_speed"} ,function(pack)
        local world = core.grun:get_scene()
        local obj = self()
        obj.name = pack.name
        obj:__synchronize__(pack)
        world:add_object(obj)
    end)
end

--进行同步
function life:__synchronize__(pack)
    self.x = pack.x
    self.y = pack.y
    self.move_speed = pack.move_speed
end

function life:__synchronize_data__()
    return {
        self.name,
        self.x,
        self.y,
        self.move_speed,
    }
end

function life:create()
    self:send_request(REQUEST_TYPE_TOALL,"create",self.name,self.x,self.y)
end

function life:draw()
    love.graphics.rectangle("fill",self.x,self.y,20,20)
end

return life:reg()
