oopsys.registered_all("scene/test_scene/objects")

local ChatUser = oopsys.getcls("ChatUser")
local ChatRootUser = oopsys.getcls("ChatRootUser")

local chat = class("ChatScene",oopsys.getcls("World")){
    user = nil,
    text = "",
}

if core.core_mode == CORE_MODE_CLIENT then
    function chat:_init()
        core.network.sockport:connect_server()
        core.game_mode = GAME_MODE_NETWORK
        self.user = ChatRootUser(core.user_name)
    end

    function chat:textinput(text)
        self.text = self.text .. text
    end

    function chat:keypressed(key)
        if key == "return" then
            self.user:chat(self.text)
            self.text = ""
        elseif key == "space" then
            self.user:root_chat(self.text)
            self.text = ""
        end
    end

    function chat:draw()
        love.graphics.print(self.text)
    end
end

return chat()