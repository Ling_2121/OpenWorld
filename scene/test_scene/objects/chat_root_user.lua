local ChatUser = require"scene.test_scene.objects.chat_user"
local chat_root_user = class("ChatRootUser",ChatUser){}

function chat_root_user:__reg_netreq__()
    ChatUser.__reg_netreq__(self)
    self:create_event("root_msg",{"name","text"},function(pack)
        print(string.format("[Root]Msg>(%s): %s",pack.name,pack.text))
    end)
end


function chat_root_user:root_chat(text)
    self:send_request(REQUEST_TYPE_TOALL,"root_msg",self.name,text)
end

return chat_root_user:reg()
