local NetworkObject = require"core.module.network.network_object"

local chat_user = class("ChatUser",NetworkObject){
    name = "laohong",
}

function chat_user:__init__(name)
    self.name = name
end

function chat_user:__reg_netreq__()
    self:create_event("msg",{"name","text"},function(pack)
        print(string.format("[User]Msg>(%s): %s",pack.name,pack.text))
    end)
end

function chat_user:chat(text)
    self:send_request(REQUEST_TYPE_TOALL,"msg",self.name,text)
end

return chat_user:reg()