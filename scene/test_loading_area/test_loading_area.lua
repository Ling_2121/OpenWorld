local Scene = oopsys.getcls("Scene")
local SceneArea = oopsys.getcls("SceneArea")

local scene = class("TestLoadingAreaScene",Scene){}

function scene:_init()
    if core.core_mode == CORE_MODE_SERVER then
        local tile_idx = {
            "grassland",
            "no_tile",
            "sand",
            "soil",
            "stone",
        }
        local idx_size = #tile_idx
        for x = -3,3 do
            for y = -3,3 do
                local area = self:get_area(x,y)
                local tn = tile_idx[love.math.random(1,idx_size)]
                area:fill_tiles("tile_default",tn)
            end 
        end
    end

    if core.core_mode == CORE_MODE_CLIENT then
    
    end
end

return scene()
