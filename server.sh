#！/bin/bash
server_ip="127.0.0.1"
server_port="23333"
echo "input server ip :"
read server_ip
echo "input server port:"
read server_port

if [$server_ip = ""] ; then
    server_ip="127.0.0.1"
fi

if [$server_port = ""] ; then
    server_port="23333"
fi
clear
echo "IP: ${server_ip}:${server_port}"
love . -mode "server" -ip ${server_ip} -port ${server_port}