local BaseObject = require"object.base_object"

local bump_object = class("BumpObject",BaseObject){
    width = 10,
    height = 10,
}

function bump_object:__init__(x,y,w,h)
    BaseObject.__init__(self,x,y)
    self.width = width or 10
    self.height = height or 10
    
    self:signal("bump")--bump(bumps)
end

function bump_object:unpack_rect()
    return self.x,self.y,self.width,self.height
end

function bump_object:move(vx,vy)
    
end

return bump_object:reg()