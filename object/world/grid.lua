local grid = class("WorldGrid",Signal){
    --区块索引位置
    aidxx = 0,
    aidxy = 0,
    --世界索引位置
    widxx = 0,
    widxy = 0,
    --世界位置
    wx = 0,
    wy = 0,

    tile_group = nil,
    tile_name = nil,
    objects = {},
}

function grid:__init__(x,y)
    self.aidxx = x or 0
    self.aidxy = y or 0
    self:signal("click")
    self:signal("object_enter")
    self:signal("object_exit")
end

function grid:add(object)
    self.objects[object] = object
    if oopsys.is(object,"BaseObject") then
        object:emit_signal("enter_grid",self)
    end
    self:emit_signal("object_enter",object)
end

function grid:remove(object)
    if self.objects[object] then
        self.objects[object] = nil
        if oopsys.is(object,"BaseObject") then
            object:emit_signal("exit_grid",self)
        end
        self:emit_signal("object_exit",object)
    end
end

return grid