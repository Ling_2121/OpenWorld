local grid = require"object.world.grid"
local Signal = oopsys.getcls("Signal")
local SerObject = oopsys.getcls("SerObject")

local area = class("WorldArea",Signal,SerObject){
    status = AREA_STATUS_STOP,
    x = 0,
    y = 0,
    ix = 0,
    iy = 0,
    objects = {},
    grids = {},
}

function area:__init__(x,y)
    x = x or 0
    y = y or 0
    self.ix = x
    self.iy = y
    self.x = x * WORLD_AREA_PSIZE
    self.y = y * WORLD_AREA_PSIZE
    self.depth = self.y

    local bgwix,bgwiy = x * WORLD_AREA_SIZE,y * WORLD_AREA_SIZE
    for x = 1,WORLD_AREA_SIZE do
        self.grids[x] = {}
        for y = 1,WORLD_AREA_SIZE do
            local g = {
                tile_group = "tile_default",
                tile_name = "no_tile",
                aidxx = x,
                aidxy = y,
                widxx = bgwix + x - 1,
                widxy = bgwiy + y - 1,
            }
            self.grids[x][y] = g
        end
    end

    self:signal("add_object")
    self:signal("remove_object")
end

function area:get_grid(x,y)
    x = math.min(WORLD_AREA_SIZE,math.max(1,x))
    y = math.min(WORLD_AREA_SIZE,math.max(1,y))
    return self.grids[x][y]
end

function area:set_tile(tile_group,tile_name,x,y)
    local g = self:get_grid(x,y)
    g.tile_group = tile_group
    g.tile_name = name
end

function area:_get_mouse_grid_position()
    local mx,my = core.get_mouse_position()
    local x,y = mx - self.x,my - self.y
    return math.ceil(x / WORLD_GRID_PSIZE),math.ceil(y / WORLD_GRID_PSIZE)
end

function area:_get_mouse_grid()
    return self:get_grid(self:_get_mouse_grid_position())
end

function area:add(object)
    local x,y = object.x - self.x,object.y - self.y
    --local gx,gy = math.ceil(x / WORLD_GRID_PSIZE), math.ceil(y / WORLD_GRID_PSIZE)
    object.name = object.name or object

    if self.objects[name] == nil then
        self.objects[object.name] = object
        object._area = self
        self:emit_signal("add_object",object)
    end
end

function area:remove(object)
    object.name = object.name or object
    if self.objects[object.name] ~= nil then
        self.objects[object.name] = nil
        object._area = nil
        self:emit_signal("remove_object",object)
    end
end

function area:draw()
    local bx,by = self.x,self.y
    for x = 1,WORLD_AREA_SIZE do
        for y = 1,WORLD_AREA_SIZE do
            local grid = self.grids[x][y]
            local group = core.assets.group(grid.tile_group)
            if group then
                local assets = group:get_assets(grid.tile_name)
                love.graphics.draw(assets,grid.widxx * WORLD_GRID_PSIZE,grid.widxy * WORLD_GRID_PSIZE)
            end        
        end
    end

    if DEBUG then
        love.graphics.setColor(1,0,0,1)
        love.graphics.rectangle("line",self.x,self.y,WORLD_AREA_PSIZE,WORLD_AREA_PSIZE)
        love.graphics.setColor(1,1,1,1)
    end
end

return area:reg()