local base_object = component("BaseObject",Signal,SerObject){
    name = "LaoHong",
    x = 0,
    y = 0,
    fsm = nil,
    _area = nil,
}:reg()

function base_object:__init__(x,y)
    self:set_position(x or 0,y or 0)
    self.fsm = FSM()
    self:signal("enter_area")
    self:signal("exit_area")
end

function base_object:set_position(x,y)
    self.x = x or self.x
    self.y = y or self.y
end

function base_object:get_position()
    return self.x,self.y
end



return base_object
