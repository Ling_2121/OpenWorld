local NetworkObject = require"core.module.network.network_object"
local world_network_object = component("WorldNetworkObject",NetworkObject){}

function world_network_object:__reg_netreq__()
    --从世界创建
    self:create_event("create",{"self"},function(pack)end)
    --从世界删除
    self:create_event("delete",{"self"},function(pack)end)
    --同步
    self:create_event("synchronize",{"name"} ,function(pack)
        local world = core.grun:get_scene()
        local obj = self()
        obj.name = pack.name
        obj:__synchronize__(pack)
        world:add_object(obj)
    end)
end

--返回同步的数据
function world_network_object:__synchronize_data__()
    return {}
end

--进行同步
function world_network_object:__synchronize__(pack)
    
end

return world_network_object:reg()