local function base_init()
    require"base" 
    oopsys.registered_all("base")
    
    require"core.core"
    oopsys.registered_all("core")
    oopsys.registered_all("object")

    core:init()
end

return function()
    base_init()
    load_tiles()

    local start_scene = require"scene.start"
    local test_chat_scene = require"scene.test_scene.chat"
    core.grun:add_scene("start",start_scene)
    core.grun:add_scene("test_chat_scene",test_chat_scene )
    core.grun:change_scene("test_chat_scene")
end

