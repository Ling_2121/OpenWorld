
core = nil
local function load_tiles()
    local AssetsGrouptile = require"core.module.assets.assets_group_tile"
    local AssetsGrouptile = oopsys.getcls("AssetsGrouptile")
    core.assets:add_group("tile_default",AssetsGrouptile()):auto_load("assets/image/tile/default")
end

function love.load(args)
    require"base/oopsys"
    require"core/const"

    oopsys.registered_all("base")
    core = require"core.core"()
    core:perp_init()

    local core_mode = CORE_MODE_CLIENT
    local network_ip = "127.0.0.1"
    local network_port = 23333
    local user_name = ""

    for i = 1,#args do
        local str = args[i]
        if str == "-mode" then
            i = i + 1
            local mode = args[i]
            if mode == "client" then
                core_mode = CORE_MODE_CLIENT
            elseif mode == "server" then
                core_mode = CORE_MODE_SERVER
            else
                assert(false, "No such mode  ("..mode..")")
            end
        elseif str == "-ip" then
            i = i + 1
            local ip = args[i]
            network_ip = ip
        elseif str == "-port" then
            i = i + 1
            local port = args[i]
            network_port = tonumber(port)
        elseif str == "-name" then
            i = i + 1
            local name = args[i]
            user_name = name
        end
    end
    core.core_mode = core_mode
    core.network_ip = network_ip
    core.network_port = network_port
    if core.core_mode == CORE_MODE_CLIENT then
        core.user_name = user_name
    end
    core:init()

    core.grun:start()
    oopsys.registered_all("core/module")
    oopsys.registered_all("object")

    load_tiles()
    --local start_scene = require"scene/start"
    --local test_chat_scene = require"scene/test_scene/chat"
    --local test_scene2 = require"scene/test_scene2/test_scene2"

    --test_scene2:connect("not_area",test_scene2,"__not_area__")

    --core.grun:add_scene("start",start_scene)
    --core.grun:add_scene("test_chat_scene",test_chat_scene )
    --core.grun:add_scene("test_scene2",test_scene2)
    --core.grun:change_scene("test_scene2")
end


