local network = class("Network"){
    sockport = nil,
    peers = {},
    event_registry = {},
}

function network:registered_event(name)
    local event = {}
    local registry = {
        request  = {run = nil,schemas = nil},
        feedback = {run = nil,schemas = nil},
    }
    self.event_registry[name] = registry
    event.request = function(self,schemas,func)--服务器收到请求时
        if core.core_mode == CORE_MODE_SERVER then
            registry.request.run = func
            registry.request.schemas = schemas
            self.sockport.sock:setSchema(name,schemas)
            self.sockport.sock:on(name,function(pack,peer,orgn_pack)
                local type = pack.pack_type
                local peers = func(pack,peer,peers)
                -- if type == REQUEST_TYPE_TOALL then--反馈在所有端执行
                --     self.sockport:broadcast_request(name,orgn_pack)
                -- elseif type == REQUEST_TYPE_TOSERVER then--反馈只在服务器和对等端执行
                --     peer:send(name,orgn_pack)
                -- elseif type == REQUEST_TYPE_SELECT then--选择性发送反馈
                --     for k,peer in ipairs(peers) do
                --         peer:send(name,orgn_pack)
                --     end
                -- end
            end)
        end
        return self
    end

    event.feedback = function(self,schemas,func)--客户端执行的反馈
        registry.feedback.run = func
        registry.feedback.schemas = schemas
        if core.core_mode == CORE_MODE_CLIENT then
            self.sockport.sock:setSchema(req_name,schemas)
            self.sockport.sock:on(name,function(pack,peer,orgn_pack)
                func(pack,peer)
            end)
        end
        return self
    end

    event.reg = function(self)
        assert(registry.request.run ~= nil,"必须定义请求！")
        if registry.feedback.run == nil then
            self:feedback(registry.request.schemas,registry.request.run)
        end
    end

    return event
end

function network:send_event(type,name,pack)
    
end

return network
