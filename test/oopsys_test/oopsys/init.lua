local _class = require"oopsys.class"
class = _class.class
component = _class.component

oopsys = {
    class = {},
}

local function get_all_file_item(dir,tb)
    tb = tb or {}
    local fitem = love.filesystem.getDirectoryItems(dir)

    local match_table = {
        {"([0-9a-zA-z_]+)%.lua", "lua_file",".lua"},-- lua文件       lua_file
        {"([0-9a-zA-z_]+%.png)", "image",".png"},-- 图像文件      image
        {"([0-9a-zA-z_]+%.ogg)", "audio",".ogg"},-- 音频          audio
        {"([0-9a-zA-z_]+%.otf)", "font",".otf"} -- 字体          font
    }

    for k,name in ipairs(fitem) do
        local p = dir.."/"..name
        local type =  love.filesystem.getInfo(p).type

        for i,matstr in ipairs(match_table) do
            local mat = matstr[1]
            local t = matstr[2]
			local n = name:match(mat) 
			if n then
                table.insert(tb,{
                    name = name,
                    type = t,
                    path = dir .. "/" .. n
				})
				break
            end
        end

        if type == "directory" then
            get_all_file_item(p,tb)
        end
	end

    return tb
end

function oopsys.getcls(name)
    return oopsys.class[name]
end

function oopsys.instance(name,...)
    local cls = oopsys.class[name]
    if cls then
        return  cls(...)
    end
end

function oopsys.is(cls,type)
    return cls.__merges__[type] ~= nil
end

function oopsys.registered(cls)
    if type(cls) == "table" and cls.__regname__ ~= nil then
        local name = cls.__regname__
        if type(cls.__regname__) == "number" then
            name = cls.__name__
        end
        oopsys.class[name] = cls
    end
end

function oopsys.registered_all(path)
    local items = get_all_file_item(path or "/")
    for i,item in ipairs(items) do
        if item.type == "lua_file" then
            require(item.path)
        end
    end
end

return oopsys