# oopsys

>   Lua面对对象系统，添加了**class**和**component**的概念

### 类

<span id = "创建类">**创建类**</span>可以用 **class**函数，当然，要先导入oopsys模块

``` lua
require"oopsys"
```

创建一个空类，class函数的第一个参数是类名（用于注册和类型识别的）

```lua
local people = class("People" ){
    	
}
```

可以事先声明成员

```lua
local people = class("People" ){
    name = "XiaoMing",
    age=18,
}
```

类的构造函数是\__init__(),对类进行实例化时会自动调用

```lua
function people:__init__(name,age)
	self.name = name or "XiaoMing"
    self.age  = age or 18
end
```

给类定义成员函数

```lua
function people:print_info()
	print(string.format("Name : %s    Age:%d",self.name,self.age))
end
```

**实例化类**

``` lua
local xiaohong = people("XiaoHong",18)
```

调用成员函数

```lua
xiaohong:print_info()
--> Name: XiaoHong    Age:18
```

**类的继承**

class函数类名后面跟其他类就可以进行继承（后面的参数是不定长的，也就是说可以继承多个类）

```lua
local vector1 = class("Vector1"){
    x = 0
}

function vector1:__init__(x)
	self.x =x or 0
end

function vector1:unpack()
	return self.x
end

local vector2 = class("Vector2",vector1){--这样，vector2就继承了vector1
    y = 0,
}

function vector2:__init__(x,y)
	vector1.__init__(self,x)--执行vector1的个构造函数
    self.y = y or 0
end

function vector2:unpack()--可以覆盖继承类的所有东西
	return self.x,self.y
end
```

###  组件

组件和类一样，但有一点不同，那就是组件无法进行实例化。创建组件可以用**component**函数，使用方法和**class**相同

这里我们给people类加个说话的组件

```lua
local people_speak = component("Speak"){}

function people_speak:speak(text)
    --得益于lua的table，所以我们不必担心有没有name成员～
	print(string.format("%s ：%s",self.name,text))
end
```

现在我们改进下people类给people类加个说话的功能

```lua
local people = class("People",people_speak){
    name = "XiaoMing",
    age=18,
}
--现在people可以说话了
```

老鸿了小鸿的对话：

```lua
local laohong = people("老红",98)
local xiaohong = people("小红",18)

laohong:speak("小红阿，爷爷给你讲个故事，要不要听要呀？")
xiaohing:speak("不听")
laohong:speak(".....")

-->老红：小红阿，爷爷给你讲个故事，要不要听要呀？
-->小红：不听
-->老红：.....
```

### 类型判断

类型判断可以用**oopsys.is(cls,type)**函数进行

```lua
--一个原始的人类，不能说话
local base_people = class("BasePeople"){
    name = "XiaoMing",
    age=18,
}

--一个超级人类，可以说话
local super_people = class("SuperPeople",base_people,people_speak){}

local xiaohong = base_people("XiaoHong",1)
local laohong = super_people("LaoHong",23333)

print(oopsys.is(xiaohong ,"BasePeople"))-->true
print(oopsys.is(xiaohong ,"SuperPeople"))-->false

print(oopsys.is(laohong ,"BasePeople"))-->true
print(oopsys.is(laohong ,"SuperPeople"))-->true
```

### 类注册

类注册有两个函数：

**oopsys.registered(cls)**:注册单个类，把类传入即可

**oopsys.registered(path)**:注册path路径下所有类（要在类外执行reg()函数）

```lua
-- objects/base_people.lua
local base_people = class("BasePeople"){
    name = "XiaoMing",
    age=18,
}

--可以定义__reg__成员，它会在类注册时执行
--可以有参数，在reg函数那传入即可
function base_people:__reg__()
    print("registered BasePeople)
end

--只有执行类的reg()函数才会自动进行注册
--reg()函数返回的类本身
return base_people:reg()
```

对类进行注册

```lua
--main.lua
oopsys.oopsys.registered("object/")
```

注册完毕，可以用 **oopsys.getcls(cls_name)** 函数获取类

```lua
local base_people = oopsys.getcls("BasePeople")
```

>   更多细节请看[base/oopsys/init.lua] 和 [base/oopsys/class.lua]