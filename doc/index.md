<<<<<<< HEAD
### OpenWorld

> 这是一个开放世界开放创作的游戏
>
> 玩家可以和朋友联机游玩
>
> 玩家可以为游戏制作和添加模组（通过lua语言）

### 基本架构

整个架构大体分为3层：

·第一层是**base**，这一层时最基础层，提供一些基本的功能

·第二层是**core**，这一层是游戏运行时的核心，提供一些基本的系统

·第三层是**object**，这是游戏的具体内容



#### 各层大体组成

### base

> 运行时的基本组件

| 名称                             | 描述              |
| -------------------------------- | ----------------- |
| [opsys](base/oopsys.md)          | lua的面对对象系统 |
| [signal](base/signal.md)         | 信号机制          |
| [depth_list](base/depth_list.md) | 带深度属性的链表  |
| [tileset](base/tileset.md)       | 瓷砖集合          |
| [fsm](base/fsm.md)               | 有限状态机        |

### core

> 运行核心

| 名称                                              | 描述           |
| ------------------------------------------------- | -------------- |
| [core](core/core.md)                              | 游戏核心       |
| [core.assets](core/module/assets/assets.md)       | 资源管理器     |
| [core.filsys](core/module/filesys/filesys.md)     | 文件系统       |
| [core.grun](core/module/grun/grun.md)             | 游戏运行管理器 |
| [core.input](core/module/input/input.md)          | 输入系统       |
| [core.registry](core/module/registry/registry.md) | 注册表         |
| [core.network](core/module/network/network.md)    | 网络管理器     |



### object

> 游戏对象

| 名称  | 描述                   |
| ----- | ---------------------- |
| block | 一些地图上的地形类对象 |
| item  | 游戏中的物品           |
| life  | 游戏中的生物           |
| world | 游戏世界               |

####  面对对象系统

lua语言本身没有面对对象的概念，但提供了可以模拟面对对象系统的方法，我为此设计了一套基于lua的面对对象系统，整个游戏都基于此。具体请看[oopsys](base/oopsys.md)

#### 信号机制

信号机制是个很方便的的系统。具体请看 [signal](base/signal.md)

=======
### OpenWorld

> 这是一个开放世界开放创作的游戏
>
> 玩家可以和朋友联机游玩
>
> 玩家可以为游戏制作和添加模组（通过lua语言）

### 基本架构

整个架构大体分为3层：

·第一层是**base**，这一层时最基础层，提供一些基本的功能

·第二层是**core**，这一层是游戏运行时的核心，提供一些基本的系统

·第三层是**object**，这是游戏的具体内容



#### 各层大体组成

### base

> 运行时的基本组件

| 名称                             | 描述              |
| -------------------------------- | ----------------- |
| [oopsys](base/oopsys.md)         | lua的面对对象系统 |
| [signal](base/signal.md)         | 信号机制          |
| [depth_list](base/depth_list.md) | 带深度属性的链表  |
| [tileset](base/tileset.md)       | 瓷砖集合          |
| [fsm](base/fsm.md)               | 有限状态机        |

### core

> 运行核心

| 名称                                              | 描述           |
| ------------------------------------------------- | -------------- |
| [core](core/core.md)                              | 游戏核心       |
| [core.assets](core/module/assets/assets.md)       | 资源管理器     |
| [core.filsys](core/module/filesys/filesys.md)     | 文件系统       |
| [core.grun](core/module/grun/grun.md)             | 游戏运行管理器 |
| [core.input](core/module/input/input.md)          | 输入系统       |
| [core.registry](core/module/registry/registry.md) | 注册表         |
| [core.network](core/module/network/network.md)    | 网络管理器     |



### object

> 游戏对象

| 名称  | 描述                   |
| ----- | ---------------------- |
| block | 一些地图上的地形类对象 |
| item  | 游戏中的物品           |
| life  | 游戏中的生物           |
| world | 游戏世界               |

####  面对对象系统

lua语言本身没有面对对象的概念，但提供了可以模拟面对对象系统的方法，我为此设计了一套基于lua的面对对象系统，整个游戏都基于此。具体请看[oopsys](base/oopsys.md)

#### 信号机制

信号机制是个很方便的的系统。具体请看 [signal](base/signal.md)

>>>>>>> 6234ce75c00932521fa4cd739997fdfdb11f9669
